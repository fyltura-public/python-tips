# Python Tips

Is a collection of useful Python snippets I regularily searched for. This is the place to remember them :)

* [dictionaries](dicts.md)

## Django

* [redirects](https://realpython.com/django-redirects) (use a redirect view with `from django.shortcuts import redirect`)


## Other sources

... you might want to have a look at

* These docs were written with [ReText](https://github.com/retext-project/retext) and the gitlab webeditor
* [Muhammad Yasoob Ullah Khalid's Intermediate Python Tips](https://book.pythontips.com/en/latest/index.html)

## License

All this is under [GPL V3](LICENSE)
