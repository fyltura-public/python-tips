# Dictionaries

## Merge two dicts

* Use [ChainMaps](https://docs.python.org/3.5/library/collections.html#collections.ChainMap)

```python
from collections import ChainMap

d1 = {1: 'one', 2: 'two'}
d2 = {3: 'three'}
ds = [d1, d2]
dict(ChainMap(*ds))
{1: 'one', 2: 'two', 3: 'three'}
```
